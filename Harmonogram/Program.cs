﻿using System;
using Domain;
using Domain.Services;

namespace Harmonogram
{
    class Program
    {
        static void Main(string[] args)
        {
            var userGenerator = new UserGenerator();
            var monthGenerator = new MonthGenerator();
            var scheduleService = new ScheduleService(userGenerator, monthGenerator);
            var reportQueryHandler = new ReportQueryHandler(scheduleService);
            var leaveCommandHandler = new LeaveCommandHandler(scheduleService);
            var january = 1;
            var year = 2017;
            var report = reportQueryHandler.GenerateReport(january, year);
            Console.Write(report);

            //wejście 
            Console.WriteLine("Proszę podać identyfikator użytkownika (1-4)");
            Int32.TryParse(Console.ReadKey().KeyChar.ToString(), out int id);
            Console.WriteLine("Proszę podać dzień urlopu w styczniu, albo nacisnąć enter:");
            Int32.TryParse(Console.ReadLine(), out int date);
            leaveCommandHandler.AddLeaveDay(id, date);
            report = reportQueryHandler.GenerateReport(january, year);
            Console.Write(report);
            Console.ReadKey();
        }
    }
}
