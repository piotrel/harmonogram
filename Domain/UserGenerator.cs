﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class UserGenerator : IUserGenerator
    {
        public List<User> GenerateUsers()
        {
            var User1 = new User()
            {
                Id = 1,
                Leaves = new List<DateTime>
                {
                    Convert.ToDateTime("03/01/2017"),
                    Convert.ToDateTime("13/01/2017")
                },
                Name = "Sławek",
                Shifts = new List<DateTime> { }
            };

            var User2 = new User()
            {
                Id = 2,
                Leaves = new List<DateTime>
                {
                    Convert.ToDateTime("04/01/2017"),
                    Convert.ToDateTime("05/01/2017"),
                    Convert.ToDateTime("14/01/2017"),
                    Convert.ToDateTime("15/01/2017")
                },
                Name = "Marcin",
                Shifts = new List<DateTime>{ }
            };

            var User3 = new User()
            {
                Id = 3,
                Leaves = new List<DateTime>
                {
                    Convert.ToDateTime("26/01/2017"),
                    Convert.ToDateTime("29/01/2017")
                },
                Name = "Kewałs",
                Shifts = new List<DateTime> { }
            };

            var User4 = new User()
            {
                Id = 4,
                Leaves = new List<DateTime>
                {
                    Convert.ToDateTime("03/01/2017"),
                    Convert.ToDateTime("02/01/2017"),
                    Convert.ToDateTime("04/01/2017"),
                    Convert.ToDateTime("05/01/2017")
                },
                Name = "Nicram",
                Shifts = new List<DateTime> { }
            };

            var userList = new List<User>
            {
                User1,
                User2,
                User3,
                User4
            };

            return userList;
        }
    }

    public interface IUserGenerator
    {
        List<User> GenerateUsers();
    }
}
