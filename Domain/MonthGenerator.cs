﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class MonthGenerator : IMonthGenerator
    {
        public List<DateTime> GenerateDaysOfMonth(int month, int year)
        {
            var datesInMonth = new List<DateTime> { };

            for (int day = 1, monthsLength = DateTime.DaysInMonth(year, month); day < monthsLength; day++)
            {
                DateTime dayI = new DateTime(year, month, day);

                if (dayI.DayOfWeek != DayOfWeek.Saturday && dayI.DayOfWeek != DayOfWeek.Sunday && !IsHoliday(dayI))
                {
                    datesInMonth.Add(dayI);
                }
            }

            return datesInMonth;
        }

        //z internetu - sprawdzone
        private bool IsHoliday(DateTime day)
        {
            if (day.Month == 1 && day.Day == 1) return true; // Nowy Rok
            if (day.Month == 1 && day.Day == 6) return true; // Trzech Króli
            if (day.Month == 5 && day.Day == 1) return true; // 1 maja
            if (day.Month == 5 && day.Day == 3) return true; // 3 maja
            if (day.Month == 8 && day.Day == 15) return true; // Wniebowzięcie Najświętszej Marii Panny, Święto Wojska Polskiego
            if (day.Month == 11 && day.Day == 1) return true; // Dzień Wszystkich Świętych
            if (day.Month == 11 && day.Day == 11) return true; // Dzień Niepodległości 
            if (day.Month == 12 && day.Day == 25) return true; // Boże Narodzenie
            if (day.Month == 12 && day.Day == 26) return true; // Boże Narodzenie
            int a = day.Year % 19;
            int b = day.Year % 4;
            int c = day.Year % 7;
            int d = (a * 19 + 24) % 30;
            int e = (2 * b + 4 * c + 6 * d + 5) % 7;
            if (d == 29 && e == 6) d -= 7;
            if (d == 28 && e == 6 && a > 10) d -= 7;
            DateTime Easter = new DateTime(day.Year, 3, 22).AddDays(d + e);
            if (day.AddDays(-1) == Easter)
                return true; // Wielkanoc (poniedziałek)
            if (day.AddDays(-60) == Easter)
                return true; // Boże Ciało
            return false;
        }
    }

    public interface IMonthGenerator
    {
        List<DateTime> GenerateDaysOfMonth(int month, int year);
    }
}
