﻿using System;

namespace Domain
{
    public class Shift
    {
        public DateTime Date { get; set; }

        public User User { get; set; }
    }
}
