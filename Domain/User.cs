﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public class User
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public List<DateTime> Shifts { get; set; }

        public List<DateTime> Leaves { get; set; }
    }
}