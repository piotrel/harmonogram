﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.Services
{
    public class ScheduleService : IScheduleService
    {
        private readonly IUserGenerator _userGenerator;
        private readonly IMonthGenerator _monthGenerator;
        private List<User> _users;
        private List<DateTime> _dates;
        private List<Shift> _shifts = null;
        private int _maxStreakCount;

        public ScheduleService(IUserGenerator userGenerator, IMonthGenerator monthGenerator)
        {
            _userGenerator = userGenerator;
            _monthGenerator = monthGenerator;
            _users = _userGenerator.GenerateUsers();
            _maxStreakCount = 3;
        }

        public List<Shift> GenerateReport(int month, int year)
        {
            _dates = _monthGenerator.GenerateDaysOfMonth(month, year);
            _shifts = new List<Shift> { };

            for (var index = 0; index < (_dates.Count() - 1); index++)
            {
                if (_users.All(u => u.Leaves.Any(l => l == _dates[index])))
                {
                    throw new Exception("Wszyscy są na urlopie dnia: " + _dates[index]);
                }

                restart:
                foreach (var user in _users)
                {
                    while (TryAssignDay(user, _dates[index]))
                    {
                        if (index < (_dates.Count() - 1))
                        {
                            index++;
                        } else
                        {
                            break;
                        }
                    }

                    if (_users.IndexOf(user) == _users.Count() - 1 && _shifts.All(s => !(s.Date == _dates[index]))) {
                        goto restart;
                    }

                    if (_shifts.Any(s => s.Date == _dates[_dates.Count() - 1]))
                    {
                        break;
                    }
                }

            }

            return _shifts;
        }
        
        public void AddLeave(int userId, int leaveIndex)
        {
            var leaveDate = new DateTime(2017, 1, leaveIndex);
            _users.Where(u => u.Id == userId).FirstOrDefault().Leaves.Add(leaveDate);
        }

        private bool IsAlreadyOn3DaysStreak(User user, DateTime dateToConsider)
        {
            var streakEndIndex = _dates.IndexOf(dateToConsider);
            var streakToCheck = streakEndIndex < 2 ? new List<DateTime> { } : CreateStreak(streakEndIndex - 1);
            if (streakToCheck.Count() == 3 && user.Shifts.Intersect(streakToCheck).Count() == 3)
            {
                if (_users.Where(u => u != user).All(l => l.Leaves.Contains(dateToConsider)))
                {
                    return false;
                }
                return true;
            }
            return false;
        }

        private List<DateTime> CreateStreak(int streakEndIndex)
        {
            var days = new List<DateTime> { };

            for (var i = 0; i < 3; i++)
            {
                var iDayIndex = streakEndIndex - i;
                if (iDayIndex < 0)
                    return days;
                days.Add(_dates[iDayIndex]);
            }
            return days;
        }
        
        private bool TryAssignDay(User user, DateTime date)
        {
            if (user.Leaves.Contains(date))
            {
                return false;
            }

            if (IsAlreadyOn3DaysStreak(user, date))
            {
                return false;
            }
            
            _shifts.Add(new Shift
            {
                Date = date,
                User = user
            });
            user.Shifts.Add(date);
            return true;
        }
    }

    public interface IScheduleService
    {
        List<Shift> GenerateReport(int month, int year);

        void AddLeave(int userId, int leaveIndex);
    }
}
