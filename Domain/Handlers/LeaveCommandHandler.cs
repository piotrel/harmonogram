﻿using Domain.Services;
using System;

namespace Domain
{
    public class LeaveCommandHandler
    {
        private readonly IScheduleService _scheduleService;

        public LeaveCommandHandler(IScheduleService scheduleService)
        {
            _scheduleService = scheduleService;
        }

        public void AddLeaveDay(int id, int keyChar)
        {
            _scheduleService.AddLeave(id, keyChar);
        }
    }
}