﻿using Domain.Services;
using System;
using System.Text;

namespace Domain
{
    public class ReportQueryHandler
    {
        private readonly IScheduleService _scheduleService;
        public ReportQueryHandler(IScheduleService scheduleService)
        {
            _scheduleService = scheduleService;
        }
        public string GenerateReport(int month, int year)
        {
            var shiftsList = _scheduleService.GenerateReport(month, year);
            var sb = new StringBuilder();
            shiftsList.ForEach(s => sb.AppendLine(s.Date.ToShortDateString() + ',' + s.User.Name.ToString()));
            return sb.ToString();
        }
    }
}